/**
 * Created by mariostzakris on 15/08/15.
 */

var mbSmsModule = require('./lib/mbsms');

var MBMessageBirdMessage = require('./lib/messagebird/mbmessagebirdmessage');

var MBMessageBirdMessageClass = MBMessageBirdMessage.MBMessageBirdMessage;
var MBMessageBirdMessageTypes = MBMessageBirdMessage.MESSAGE_TYPES;

var MBMessageBirdRecipient = require('./lib/messagebird/mbmessagebirdrecipient').MBMessageBirdRecipient;

var CLASS_TYPES = {

    MESSAGEBIRD: 'messagebird'

};


/**
 * Expose interface for this module
 */
module.exports = {
    mbSmsInstance: mbSmsModule.mbSmsInstance,
    mbSmsClassTypes: CLASS_TYPES,
    sendSmsWithMessage: function (mbMessage, callback) {

        mbSmsModule.sendSmsWithMessage(mbMessage, callback);

    },
    getClassesFactory: function (type) {

        /**
         * Expose all classes needed when one needs messagebird (or later something else):
         */

        switch (type) {

            case CLASS_TYPES.MESSAGEBIRD:

                return {
                    MessageClass: MBMessageBirdMessageClass,
                    MessageTypes: MBMessageBirdMessageTypes,
                    RecipientClass: MBMessageBirdRecipient
                };


                break;

            default:
                throw new Error("Unhandled type");
        }

    }

};