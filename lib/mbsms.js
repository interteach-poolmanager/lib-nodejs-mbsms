/**
 * Created by ivocazemier on 10/08/15.
 */

//Native:
var util = require('util');
var async = require('async');
var EventEmitter = require('events').EventEmitter;

//Third Party:


//Own:
//var MBMessage = require('./base/mbmessage').MBMessage;
var EVENTS = require('../mbsmsworker').WORKER_EVENTS;


function MBSmsError() {
    Error.call(this);
}

util.inherits(MBSmsError, Error);

// Constructor
function MBSms() {


}
util.inherits(MBSms, EventEmitter);

/**
 * The message will be queued eventually
 * Before we queue the message, the content is checked on:
 * - Consistency of the attachments (if available online)
 *
 *
 * @param message
 * @param callback
 * @returns {*}
 */
MBSms.prototype.sendSmsWithMessage = function (message, callback) {

    var self = this;
    self.emit(EVENTS.MESSAGE_SENT, message);
    return callback(null, message);

};

var mbSmsInstance = new MBSms();

module.exports = {
    mbSmsInstance: mbSmsInstance,
    sendSmsWithMessage: function (mbMessage, callback) {

        mbSmsInstance.sendSmsWithMessage(mbMessage, callback);

    }
};