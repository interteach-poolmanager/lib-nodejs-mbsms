/**
 * Created by ivocazemier on 14/08/15.
 */

var _ = require('lodash');

var mbMessageBirdConstants = require('./mbmessagebirdconstants');


var DATETIME_FORMATS = mbMessageBirdConstants.DATE_TIME_FORMATS;


var MESSAGE_TYPES = {

    SMS: 'sms',
    BINARY: 'binary',
    PREMIUM: 'premium'

};


var MCLASS_TYPE = {
    FLASH: 0,
    NORMAL: 1
};

var DATA_ENCODING = {

    UNICODE: 'unicode',
    PLAIN: 'plain'

};


function MBMessageBirdMessage() {

    // An unique random ID which is created on the MessageBird platform and is returned upon creation of the object.
    this.id = null;

    // The URL of the created object.
    this.href = null;

    // Tells you if the message is sent or received.
    // mt: mobile terminated (sent to mobile)
    // mo: mobile originated (received from mobile)
    this.direction = null;

    // The type of message. Values can be: sms, binary, premium, or flash
    this.type = null;

    // The sender of the message. This can be a telephone number (including country code) or an alphanumeric string. In case of an alphanumeric string, the maximum length is 11 characters.
    this.originator = null;

    // The body of the SMS message.
    this.body = null;

    // A client reference
    this.reference = null;

    // The amount of seconds that the message is valid. If a message is not delivered within this time, the message will be discarded.
    this.validity = null;

    // The SMS route that is used to send the message.
    this.gateway = null;

    // A hash with extra information. Further explanation in the table below.
    this.typeDetails = null;

    // The datacoding used, can be plain or unicode
    this.datacoding = null;

    // Indicated the message type. 1 is a normal message, 0 is a flash message.
    this.mclass = null;

    // The scheduled date and time of the message in RFC3339 format (Y-m-d\TH:i:sP)
    this.scheduledDatetime = null;

    // The date and time of the creation of the message in RFC3339 format (Y-m-d\TH:i:sP)
    this.createdDatetime = null;

    // The hash with recipient information. Further explanation in the table below.
    this.recipients = null;


}

/**
 *
 * When we send a message, this method is called upon JSON.stringify().
 * @param key
 * @throws Error
 */
MBMessageBirdMessage.prototype.toJSON = function (key) {

    var messageObject = new Object();


    /*********************
     * Required:
     *********************/

    // The sender of the message.
    // This can be a telephone number (including country code) or an alphanumeric string.
    // In case of an alphanumeric string, the maximum length is 11 characters.
    if (_.isString(this.originator)) {
        messageObject['originator'] = this.originator;
    } else {
        throw new Error('No originator set?');
    }


    // The body of the SMS message.
    messageObject['body'] = this.body;

    // TODO: How and where do we retrieve these msisdn's?
    // The array of recipients msisdn's.
    if (this.recipients instanceof Array) {
        messageObject['recipients'] = this.recipients;
    } else {
        throw new Error("No recipients set?");
    }


    /*********************
     * Optional:
     *********************/

        // The type of message. Values can be: sms, binary, premium, or flash.
        // Default: sms
    messageObject['type'] = this.type || MESSAGE_TYPES.SMS;

    // A client reference
    if (_.isString(this.reference) && this.reference.length > 0) {
        messageObject['reference'] = this.reference || "";
    }

    // The amount of seconds that the message is valid.
    // If a message is not delivered within this time, the message will be discarded.
    messageObject['validity'] = this.validity;

    // The SMS route that is used to send the message.
    messageObject['gateway'] = this.gateway;

    switch (this.type) {
        case MESSAGE_TYPES.BINARY:
        case MESSAGE_TYPES.PREMIUM:
            // An hash with extra information. Is only used when a binary or premium message is sent.
            messageObject['typeDetails'] = this.typeDetails;
            break;
    }


    // The datacoding used, can be plain or unicode
    // Default: plain
    this.datacoding = this.datacoding || DATA_ENCODING.PLAIN;
    switch (this.datacoding) {
        case DATA_ENCODING.PLAIN:
        case DATA_ENCODING.UNICODE:
            messageObject['datacoding'] = this.datacoding;
            break;
        default:
            throw new Error("Unhandled data encoding:'" + this.datacoding + "'");

    }


    // Indicated the message type. 1 is a normal message, 0 is a flash message.
    // Default: 1
    this.mclass = this.mclass || MCLASS_TYPE.NORMAL;
    switch (this.mclass) {
        case MCLASS_TYPE.FLASH:
        case MCLASS_TYPE.NORMAL:
            break;
        default:
            throw new Error("Unhandled mclass type:'" + this.mclass + "'");
    }

    messageObject['mclass'] = this.mclass;


    // The scheduled date and time of the message in RFC3339 format (PHP: Y-m-d\TH:i:sP) e.g: '2015-08-17T12:59:42+00:00'
    if (this.scheduledDatetime) {
        var scheduledDatetimeMoment = moment(this.scheduledDatetime).format(DATETIME_FORMATS.RFC3339);
        if (scheduledDatetimeMoment != "Invalid date") {
            messageObject['scheduledDatetime'] = scheduledDatetimeMoment;
        } else {
            throw new Error("scheduledDatetime has an Invalid date");
        }
    }

    return messageObject;
};


module.exports = {
    MESSAGE_TYPES: MESSAGE_TYPES,
    DATA_ENCODING: DATA_ENCODING,
    MBMessageBirdMessage: MBMessageBirdMessage
};