/**
 * Created by ivocazemier on 17/08/15.
 */

var moment = require('moment');


function MBMessageBirdRecipient(){


    //TODO: We need a string phone number => msisdn parser! e.g. '+31 622 10 22108 75 2' (string) => 316221022108752 (int)
    // The msisdn of the recipient
    this.recipient = null;

    // The status of the message sent to the recipient. Possible values: scheduled, sent, buffered, delivered, and delivery_failed
    this.status = null;

    // The datum time of the last status in RFC3339 format (Y-m-d\TH:i:sP)
    this.statusDatetime = null;


}

/**
 *
 * When we send a message, this method is called upon JSON.stringify().
 * @param key
 * @throws Error
 */
MBMessageBirdRecipient.prototype.toJSON = function(key){

    var recipient = new Object();


};

module.exports = {

    MBMessageBirdRecipient:MBMessageBirdRecipient

};