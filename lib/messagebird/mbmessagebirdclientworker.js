/**
 * Created by ivocazemier on 14/08/15.
 * Edited by mariostzakris on 19/08/15
 */

var util = require('util');
var path = require('path');

var _ = require('lodash');
var async = require('async');
var request = require('request');

var mbSMSConfig = require('../mbsmsconfig');
var packageJSON = require('../../package.json');

var EVENTS = require('../../mbsmsworker').EVENTS;

var MESSAGEBIRD_EVENTS = {
    RESPONSE_STATUS: 'response_status',
    WORKER_ERROR:'worker_error'
};

var EventEmitter = require('events').EventEmitter;


var MBMessagebirdMessage = require('./mbmessagebirdmessage').MBMessageBirdMessage;

var MESSAGEBIRD_HTTP_STATUSCODES = {
    FOUND: 200,
    CREATED: 201,
    NO_CONTENT: 204,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    METHOD_NOT_ALLOWED: 405,
    UNPROCESSABLE_ENTITY: 422
};

var WORKER_ERROR_CODES = {
    REQUEST_ERROR: 300,
    HTTP_ERROR: 400
};

var MESSAGEBIRD_REQUEST_METHODS = {
    GET: 'GET',
    POST: 'POST',
    DELETE: 'DELETE'
};


var MESSAGEBIRD_PATHS = {

    MESSAGES: "/messages",
    VOICE_MESSAGES: "/voicemessages",
    HLR: "/hlr" //Home Location Register, An HLR allows you to view which mobile number (MSISDN) belongs to what operator in real time and see whether the number is active.

};



function MBMessagebirdStatusResponse(message) {

    this.message = message;
    this.code = null;
}

function MBMessagebirdWorkerError(message) {
    Error.call(this);

    this.message = message;

}

util.inherits(MBMessagebirdWorkerError, Error);



/**
 * Default messagebird settings
 */
var defaultMessagebirdSettings = {
    accessKey: process.env.MESSAGEBIRD_ACCESS_KEY || 'MAKE YOUR OWN KEY',
    endPoint: process.env.MESSAGEBIRD_ENDPOINT || 'https://rest.messagebird.com/messages'

};


function MBMessagebirdWorkerClient() {

}

util.inherits(MBMessagebirdWorkerClient, EventEmitter);

MBMessagebirdWorkerClient.prototype.initializeWithAppRoot = function (appRootPath, callback) {
    /**
     * This method is prepared to be able to call from a separate process (dyno)
     * For now, it's implemented as EventListener (which receives events from a EventEmitter class)
     */
    var self = this;
    var appRoot = path.resolve(appRootPath);
    if (appRoot) {

        async.waterfall([

            function (callback) {

                /**
                 * First load the config file (requires app root path)
                 */
                mbSMSConfig.loadConfig(appRoot, function (error, settings) {

                    return callback(error, settings);

                });
            },
            function (settings, callback) {


                /**
                 * When settings are fetched, initialize worker
                 */
                self.initializeWorkerWithSettings(settings, function (error, result) {

                    return callback(error, result);

                });

            }

        ], function (error, settings) {

            return callback(error, settings);

        });

    } else {
        var errorMessage = "Provide path to app root, please";
        throw new Error(errorMessage);
    }

};

MBMessagebirdWorkerClient.prototype.initializeWorkerWithSettings = function (apiSettings, callback) {

    // Merge with default messagebird settings (we expect a messagebird property)
    if (apiSettings.messagebird !== undefined) {
        this.settings = _.merge(defaultMessagebirdSettings, apiSettings.messagebird);
    } else {
        this.settings = defaultMessagebirdSettings;
    }

    if (_.isString(this.settings.accessKey)) {


        //TODO:
        // We don't use a lib here, just patch the settings,
        // but when we need to initialize more, this is the place to do it.


        // Mandrill client initialized!
        return callback(null, "MBMessagebirdClientWorker initialized!");

    } else {
        var errorMessage = "Could not initialize messagebird (no api key found)";
        var error = new Error(errorMessage);
        return callback(error);
    }

};

MBMessagebirdWorkerClient.prototype.degradeWorkerGracefully = function (callback) {


};

MBMessagebirdWorkerClient.prototype.sendSMS = function (mbMessage, callback) {

    var self = this;

    if (mbMessage instanceof MBMessagebirdMessage) {
        self.executeSendSMS(mbMessage, callback);
    } else {
        var errorMessage = "mbMessage not of instance MBMessagebirdMessage";
        var workerError = new MBMessagebirdWorkerError(errorMessage);
        return callback(workerError);
    }


};


MBMessagebirdWorkerClient.prototype.executeSendSMS = function (mbMessage) {

    var self = this;
    // We only do a 'HEAD' request, to see if the server responds with 200
    // (gives a good indication about the availability of the resource)
    var message = JSON.stringify(mbMessage);
    var options = {
        method: 'POST',
        url: this.settings.endPoint,
        encoding: null,
        followRedirect: true,
        body: message,
        maxRedirects: 5,
        headers: {
            'User-Agent': 'mediaBunker mbsms version:' + packageJSON.version,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Accept-Charset': 'utf-8',
            'Authorization': 'AccessKey ' + this.settings.accessKey
        }
    };

    // Execute the request:
    request(options, function (error, response, body) {

        if (error) {
            var errorMessage = "Something went wrong with requesting a resource with error message:" + error.message;
            var workerError = new MBMessagebirdWorkerError(errorMessage);
            workerError.code = WORKER_ERROR_CODES.REQUEST_ERROR;
            self.emit (MESSAGEBIRD_EVENTS.WORKER_ERROR , workerError);


        }

        var statusMessage = null;
        var errorMessage = null;
        var response_error = false;
        switch (response.statusCode) {
            case MESSAGEBIRD_HTTP_STATUSCODES.FOUND:
                statusMessage = "Found: we found the request resource";
                break;
            case MESSAGEBIRD_HTTP_STATUSCODES.CREATED:
                statusMessage = "Created: the resource is successfuly created";
                break;
            case MESSAGEBIRD_HTTP_STATUSCODES.EMPTY:
                response_error = true;
                errorMessage = "No Content: the requested resources is empty";
                break;
            case MESSAGEBIRD_HTTP_STATUSCODES.UNAUTHORIZED:
                response_error = true;
                errorMessage = "Unauthorized: the access key was incorrect";
                break;
            case MESSAGEBIRD_HTTP_STATUSCODES.NOT_FOUND:
                response_error = true;
                errorMessage = "Not found: the resources cannot be found";
                break;
            case MESSAGEBIRD_HTTP_STATUSCODES.METHOD_NOT_ALLOWED:
                response_error = true;
                errorMessage = "Method Not Allowed: the method is not allowed";
                break;
            case MESSAGEBIRD_HTTP_STATUSCODES.UNPROCESSABLE_ENTITY:
                response_error = true;
                errorMessage = "Unprocessable Entity: the resource couldn't be created";
                break;
        }
        if(response_error){
            var workerError = new MBMessagebirdWorkerError(errorMessage);
            workerError.code = WORKER_ERROR_CODES.HTTP_ERROR;
            self.emit (MESSAGEBIRD_EVENTS.WORKER_ERROR , workerError);
        }else{
            var status = new MBMessagebirdStatusResponse(statusMessage);
            status.code = response.statusCode;
            self.emit(MESSAGEBIRD_EVENTS.RESPONSE_STATUS, status);
        }
    });


};

/////////////////TODO: SPIEKEN! WTF! "var callback???"
// Communicate
function talk(path, fields, callback) {
    if (typeof fields === 'function') {
        var callback = fields
    }
    if (typeof fields !== 'object') {
        var fields = {}
    }

    // prevent multiple callbacks
    var complete = false

    function doCallback(err, res) {
        if (!complete) {
            complete = true
            callback(err, res || null)
        }
    }

    // build request
    fields.username = module.exports.username
    fields.password = module.exports.password

    var query = querystring(fields)

    var options = {
        hostname: 'api.messagebird.com',
        path: '/api/' + path,
        method: 'POST',
        headers: {
            'User-Agent': 'messagebird.js (https://github.com/fvdm/nodejs-messagebird)',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': query.length
        }
    }

    var request = https(options)

    // response
    request.on('response', function (response) {
        var data = ''

        response.on('data', function (ch) {
            data += ch
        })
        response.on('close', function () {
            doCallback(new Error('request closed'))
        })

        response.on('end', function () {
            if (response.statusCode === 200) {
                try {
                    data = xml2json.parser(data)
                    data = data.response.item || data
                    if (!data.credits && (data.responsecode != 1 || data.responsemessage !== 'OK')) {
                        var err = new Error('api error')
                        err.statusCode = response.statusCode
                        err.responsecode = data.responsecode
                        err.responsemessage = data.responsemessage
                        doCallback(err)
                    } else {
                        doCallback(null, data)
                    }
                } catch (e) {
                    var err = new Error('api invalid')
                    err.error = e
                    doCallback(err)
                }
            } else {
                var err = new Error('api http error')
                err.statusCode = response.statusCode
                err.body = data || null
                doCallback(err)
            }
        })
    })

    // error
    request.on('error', function (error) {
        if (error.code === 'ECONNRESET') {
            var err = new Error('request timeout')
        } else {
            var err = new Error('request failed')
        }
        err.error = error
        doCallback(err)
    })

    // timeout
    request.on('socket', function (socket) {
        socket.setTimeout(module.exports.timeout)
        socket.on('timeout', function () {
            request.abort()
        })
    })

    // run it
    request.end(query)
}

var mbSmsInstance = new MBMessagebirdWorkerClient();

module.exports = {
    workerInstance: mbSmsInstance,
    MESSAGEBIRD_REQUEST_METHODS: MESSAGEBIRD_REQUEST_METHODS,
    MESSAGEBIRD_HTTP_STATUSCODES: MESSAGEBIRD_HTTP_STATUSCODES,
    MESSAGEBIRD_EVENTS: MESSAGEBIRD_EVENTS,
    sendSMS: function (mbMessage, callback) {

        mbSmsInstance.sendSMS(mbMessage, callback);

    },
    initializeWithAppRoot: function (appRootPath, callback) {

        mbSmsInstance.initializeWithAppRoot(appRootPath, callback);

    },

    initializeWorkerWithSettings: function (settings, callback) {

        mbSmsInstance.initializeWorkerWithSettings(settings, callback);

    },
    degradeWorkerGracefully: function (callback) {

        mbSmsInstance.degradeWorkerGracefully(callback);

    }
};