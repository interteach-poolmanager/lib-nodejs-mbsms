/**
 * Created by ivocazemier on 17/08/15.
 */



var DATE_TIME_FORMATS = {

    RFC3339: "YYYY-MM-DD\THH:mm:ssZ"

};


module.exports = {

    DATE_TIME_FORMATS:DATE_TIME_FORMATS

};