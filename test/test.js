/**
 * Created by mariostzakris on 18/08/15.
 *
 *
 * Test works as follows:
 * - Add a listener to the sender (which delegates the message to the worker)
 * - First initialize the worker (ready for sending sms)
 * - When worker is ready initializing, we will simulate in sending a SMS
 *
 * Prerequisites:
 * - Set environment variable: MANDRILL_APIKEY => SOME_SECRET_API_KEY
 *
 * Third Party Dependencies (see enclosed package.json):
 * - async (https://www.npmjs.com/package/async)
 * - request (https://www.npmjs.com/package/request)
 * - lodash (https://www.npmjs.com/package/lodash)
 * - file-type (https://www.npmjs.com/package/file-type)
 * - mandrill-api (https://www.npmjs.com/package/mandrill-api)
 */

// Native node module:
var path = require('path');

// Fetch path to current working directory (important for config file loading)
var cwd = path.resolve(__dirname);

// Fetch both independent modules (mailer + mailerworker)
var mbSmsModule = require('../mbsms');
var mbSmsWorkerModule = require('../mbsmsworker');

// Fetch classes for Mandrill:
var messageBirdClasses = mbSmsModule.getClassesFactory(mbSmsModule.mbSmsClassTypes.MESSAGEBIRD);

var MBMessageBirdMessage = messageBirdClasses.MessageClass;
var MBMessageBirdRecipient = messageBirdClasses.RecipientClass;

// We need the instance for now, because this is an EventEmitter (simulate the job event for queueing)
var smsInstance = mbSmsModule.mbSmsInstance;
var smsWorkerInstance = mbSmsWorkerModule.mbSmsWorkerInstance;
// Corresponding events:
var WORKER_EVENTS = mbSmsWorkerModule.WORKER_EVENTS;
var MESSAGEBIRD_EVENTS = mbSmsWorkerModule.MESSAGEBIRD_EVENTS


/**
 * Create the listener on the mailer (listen for new jobs from the virtual queue)
 *
 * Simulate the Queueing system here, (normally the worker instance is able to listen for jobs from the queue all by itself)
 * When a event arises the event is delegated to the worker and is the only place where they are connected.
 * (Loosely coupled by event, not hard referenced)
 *
 */

smsInstance.on(WORKER_EVENTS.MESSAGE_SENT, function (mbMessage) {

    mbSmsWorkerModule.sendSMS(mbMessage, function (error, result) {

        if (error) {
            console.log(error);
        } else {
            console.log("YESS!");
        }

    });

});

smsWorkerInstance.on(MESSAGEBIRD_EVENTS.RESPONSE_STATUS, function(status){
    console.log(status.message + ' - status code: ' + status.code);
});

smsWorkerInstance.on(MESSAGEBIRD_EVENTS.WORKER_ERROR, function(error){
    console.log(error.message + ' - error code: ' + error.code);
});


/**
 * Bootstrap the mailer worker
 * And when ready we simulate the sending of an email
 * TEST!
 */
mbSmsWorkerModule.initializeWorker(cwd, function (error, result) {
    if (error) {
        console.log(error);
    } else {

        console.log(result);


        // SMS system initialized:

        // Prepare a message for messagebird:

        var testMessage = new MBMessageBirdMessage();

        testMessage.originator = "MessageBird";
        testMessage.body = "Hey there, I am testing messagebird sms";
        testMessage.recipients = [31631517295];


        mbSmsModule.sendSmsWithMessage(testMessage, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                //console.log(result);
            }
        });


    }
});




