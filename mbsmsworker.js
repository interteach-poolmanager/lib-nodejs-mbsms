/**
 * Created by ivocazemier on 17/08/15.
 */

var mbMessagebirdWorkerModule = require('./lib/messagebird/mbmessagebirdclientworker');
var MBMessagebirdMessage = require('./lib/messagebird/mbmessagebirdmessage').MBMessageBirdMessage;

var WORKER_EVENTS = {
    MESSAGE_SENT: 'message_sent'
};



/**
 * Expose interface for this module:
 */
module.exports = {
    mbSmsWorkerInstance: mbMessagebirdWorkerModule.workerInstance,
    WORKER_EVENTS: WORKER_EVENTS,
    MESSAGEBIRD_EVENTS: mbMessagebirdWorkerModule.MESSAGEBIRD_EVENTS,
    sendSMS: function (mbMessage, callback) {

        if (mbMessage instanceof MBMessagebirdMessage) {

            mbMessagebirdWorkerModule.sendSMS(mbMessage, callback);

        } else {
            return callback(new Error("Unhandled message type"));
        }

    },
    initializeWorker: function (appRootPath, callback) {

        mbMessagebirdWorkerModule.initializeWithAppRoot(appRootPath, callback);

    },
    degradeWorkerGracefully: function (callback) {

        mbMessagebirdWorkerModule.degradeWorkerGracefully(callback);

    }
};